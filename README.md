**Always save your Unity project before making a commit!**

Install git lfs following: http://git-lfs.github.com/
No extra steps are required for git lfs (seems like they added fetch into git hooks).

Add Unit tests following: https://www.raywenderlich.com/9454-introduction-to-unity-unit-testing

Writing tests for Monobehaviours:
  * Write normal tests as you would for other classes.
  * Write tests for its visual appearance during its life-cycle when it is **active**, to check against sudden unnatural movement.
  * Test all possible interactions in its life-cycle, such as `enemy spawn > enemy get hit > enemy attacks > enemy dies > enemy respawn`.

Adding Monobehaviours:
  * You may add them in the Editor by drag and drop.
  * Their public/serialized fields can be set in the Editor.
  * They cannot be constructed normally. Use AddComponent<> to add it to a GameObject.
