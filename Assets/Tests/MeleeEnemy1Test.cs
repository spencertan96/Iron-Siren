﻿using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace Tests
{
    public class MeleeEnemy1Test
    {
        private MeleeEnemy1 enemy;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            GameObject.Destroy(enemy);
            SceneManager.UnloadSceneAsync("Level1");
        }

        [SetUp]
        public void SetUp()
        {
        }

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        public IEnumerator MeleeEnemy1Damaged()
        {
            yield return Load();
            float health = enemy.GetHealth();
            float damage = 10.0f;
            enemy.ApplyDamage(damage);
            Assert.AreEqual(health - damage, enemy.GetHealth());

            health = enemy.GetHealth();
            damage = health + 1.0f;
            enemy.ApplyDamage(damage);
            Assert.AreEqual(health - damage, enemy.GetHealth());

            enemy.RestoreHealth();
            Assert.AreEqual(MeleeEnemy1.InitialHealth, enemy.GetHealth());
        }

        private IEnumerator Load()
        {
            if (enemy == null)
            {
                SceneManager.LoadScene("Level1", LoadSceneMode.Additive);
                yield return null;
                GameObject go1 = GameObject.Find("Environment");
                Environment env = go1.GetComponent<Environment>();
                GameObject go = GameObject.Instantiate(env.meleeEnemyTemplate);
                go.SetActive(true);
                yield return null;
                enemy = go.GetComponentInChildren<MeleeEnemy1>();
            }
        }
    }
}
