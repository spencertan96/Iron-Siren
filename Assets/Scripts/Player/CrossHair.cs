﻿using UnityEngine;

class CrossHair
{
    private SpriteRenderer spriteRenderer;
    private const float distance = 0.5f;

    private Vector3 scale;

    public CrossHair(SpriteRenderer spriteRenderer, Sprite crossHair)
    {
        this.spriteRenderer = spriteRenderer;
        spriteRenderer.sprite = crossHair;
        spriteRenderer.color = new Color(0.1f, 0.2f, 0.9f);
        scale = spriteRenderer.transform.localScale;
    }

    public void UpdatePosition(Ray ray, Vector3 playerForward)
    {
        float scale = Vector3.Dot(ray.direction, playerForward);
        spriteRenderer.transform.position = ray.origin + distance / scale * ray.direction;
    }

    public void SetScale(bool hasTarget)
    {
        if (hasTarget)
        {
            spriteRenderer.transform.localScale = scale * 2;
        }
        else
        {
            spriteRenderer.transform.localScale = scale;
        }
    }
}
