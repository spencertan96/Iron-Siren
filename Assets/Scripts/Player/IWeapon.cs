﻿using UnityEngine;

interface IWeapon
{
    bool Point(Ray ray);
    void Fire();
}
