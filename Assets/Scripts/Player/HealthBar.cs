﻿using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    private Image image;
    private float maxHealthPoints = 100.0f;
    //private float healthBarStepsLength = 10;
    private float damageDecreaseRate = 10;
    private float visualHealth;

    private RectTransform imageRectTransform;

    private float damageToProcess;

    private float Health
    {
        get { return visualHealth; }
        set
        {
            visualHealth = Mathf.Clamp(value, 0, MaxHealthPoints);
            image.material.SetFloat("_Percent", visualHealth / MaxHealthPoints);

            if (visualHealth < Mathf.Epsilon)
                Damage = 0;
        }
    }

    private float Damage
    {
        get { return damageToProcess; }
        set
        {
            damageToProcess = Mathf.Clamp(value, 0, MaxHealthPoints);
            image.material.SetFloat("_DamagePercent", damageToProcess / MaxHealthPoints);
        }
    }

    private float MaxHealthPoints
    {
        get { return maxHealthPoints; }
        set
        {
            maxHealthPoints = value;
            //image.material.SetFloat("_Steps", MaxHealthPoints / healthBarStepsLength);
        }
    }

    protected void Awake()
    {
        image = gameObject.GetComponent<Image>();
        imageRectTransform = image.GetComponent<RectTransform>();
        image.material = Instantiate(image.material); // Clone material

        image.material.SetVector("_ImageSize", new Vector4(imageRectTransform.rect.size.x, imageRectTransform.rect.size.y, 0, 0));

        MaxHealthPoints = MaxHealthPoints; // Force the call to the setter in order to update the material
        visualHealth = MaxHealthPoints; // Force the call to the setter in order to update the material
    }

    protected void Update()
    {
        if (Damage > 0)
        {
            Damage -= damageDecreaseRate * Time.deltaTime;
        }
    }

    public void ApplyDamage(float amount)
    {
        Damage += amount;
        Health -= amount;
    }

    public void ApplyHeal(float amount)
    {
        Damage -= Mathf.Min(amount, Damage);
        Health += amount;
    }
}