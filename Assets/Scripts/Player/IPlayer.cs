﻿using UnityEngine;

public interface IPlayer
{
    void ApplyDamage(float damage);
    Vector3 GetPosition();
    Vector3 GetForwardVector();
    float GetCameraVerticalAngle();
}
