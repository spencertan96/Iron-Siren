﻿using UnityEngine;

class CrossBow: IWeapon
{
    private const float damage = 50.0f;
    private const float shotPower = 1500.0f;

    private GameObject model;
    private IEnvironment environment;
    private IRhythm rhythm;
    private IPlayerHittable target;
    private IAbility ability = new DoubleDamage();

    public CrossBow(IRhythm rhythm, GameObject model, IEnvironment environment)
    {
        this.rhythm = rhythm;
        this.model = model;
        this.environment = environment;
    }

    public bool Point(Ray ray)
    {
        Vector3 hitPosition;
        target = environment.TestHit(ray, out hitPosition);
        model.transform.LookAt(hitPosition);
        return target != null;
    }

    public void Fire()
    {
        if (!rhythm.OnBeat())
        {
            return;
        }
        Transform arrowTransform = model.transform.GetChild(1);
        environment.CreateArrow(arrowTransform.gameObject, arrowTransform, shotPower);
        bool hasBonus = rhythm.FireWeapon();
        ability.ApplyBonus(hasBonus, damage, target);
    }
}
