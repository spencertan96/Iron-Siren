﻿using UnityEngine;

public class Player : MonoBehaviour, IPlayer
{
    public Camera playerCamera;
    public GameObject weaponModel;
    public Sprite crossHairSprite;
    public Environment environment;
    public SpriteRenderer spriteRenderer;
    public HealthBar healthBar;
    public ALevel level;

    private CrossHair crossHair;
    private IWeapon weapon;
    private float health;

    // Start is called before the first frame update
    void Awake()
    {
        crossHair = new CrossHair(spriteRenderer, crossHairSprite);
        weapon = new CrossBow(level.GetRhythm(), weaponModel, environment);
        playerCamera.ScreenToWorldPoint(new Vector3(playerCamera.scaledPixelWidth, playerCamera.scaledPixelHeight, 0.05f));
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = playerCamera.ScreenPointToRay(Input.mousePosition);
        bool hasTarget = weapon.Point(ray);
        crossHair.UpdatePosition(ray, transform.forward);
        crossHair.SetScale(hasTarget);
        HandleClick();
    }

    public void ApplyDamage(float damage)
    {
        health -= damage;
        healthBar.ApplyDamage(damage);
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }

    public Vector3 GetForwardVector()
    {
        return transform.forward;
    }

    public float GetCameraVerticalAngle()
    {
        return Mathf.Atan(Mathf.Tan(playerCamera.fieldOfView) * playerCamera.aspect);
    }

    private void HandleClick()
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {
            weapon.Fire();
        }
    }
}
