﻿interface IAbility
{
    void ApplyBonus(bool hasBonus, float damage, IPlayerHittable enemy);
}
