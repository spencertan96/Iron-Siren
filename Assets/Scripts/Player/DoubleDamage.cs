﻿class DoubleDamage : IAbility
{
    public DoubleDamage()
    {
        ;
    }

    public void ApplyBonus(bool hasBonus, float damage, IPlayerHittable enemy)
    {
        if (hasBonus)
        {
            enemy?.ApplyDamage(damage * 2);
        }
        else
        {
            enemy?.ApplyDamage(damage);
        }
    }
}
