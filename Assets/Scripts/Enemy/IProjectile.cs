﻿using UnityEngine;

interface IProjectile : IPlayerHittable
{
    void ResetProjectile(float fullTime, Vector3 originalPosition, Vector3 destination, Vector3 up, IPlayer player);
}
