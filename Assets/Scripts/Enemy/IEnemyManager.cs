﻿public interface IEnemyManager
{
    bool AreEnemiesDead();
    void CreateNewEnemies();
    void PlayBeatAnimation();
}
