﻿using UnityEngine;

public interface IEnemy : IPlayerHittable
{
    void AddWaypoint(Vector3 point);
    void AddPatrolPoints(Vector3[] points);
    void Attack();
    void Die();
    void PlayBeatAnimation();
}
