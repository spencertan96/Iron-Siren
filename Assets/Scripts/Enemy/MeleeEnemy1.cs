﻿using System.Collections.Generic;
using UnityEngine;

public class MeleeEnemy1 : MonoBehaviour, IEnemy
{
    public Player target;
    //public Sprite sprite;
    public const float InitialHealth = 100.0f;

    private const float damage = 3.0f;
    private const float rotationSpeed = 100.0f;
    private const float moveSpeed = 0.05f;
    private const float eps = 0.1f;
    private const float attackCooldown = 2.0f;

    private Transform p_transform
    {
        get
        {
            return transform.parent;
        }
    }

    private Animator animator;
    private Vector3 lookAtTarget;
    private bool isDying = false;
    private float timeLeftToAttack = 0.0f;
    private List<Vector3> wayPoints = new List<Vector3>();
    private float health = InitialHealth;

    void Update()
    {
        if (isDying)
        {
            return;
        }
        StartAttack();
        GradualRotation(Time.deltaTime);
    }

    public float GetHealth()
    {
        return health;
    }

    public void RestoreHealth()
    {
        wayPoints.Clear();
        health = InitialHealth;
        isDying = false;

        gameObject.SetActive(true);
        gameObject.layer = Environment.CollisionLayer;
        animator = gameObject.GetComponentInChildren<Animator>();
        animator.applyRootMotion = false;
        //Apply inputs to animator
        animator.Play("Run");

        lookAtTarget = transform.position + transform.forward;
    }

    public void AddWaypoint(Vector3 point)
    {
        wayPoints.Add(point);
    }

    public void AddPatrolPoints(Vector3[] points)
    {
        throw new System.NotImplementedException();
    }

    public void ApplyDamage(float damage)
    {
        if (isDying)
        {
            return;
        }
        health -= damage;
        if (health <= 0)
        {
            animator.Play("Death");
            isDying = true;
            gameObject.layer = Environment.IgnoreLayer;
        }
    }

    public void Attack()
    {
        target.ApplyDamage(damage);
    }

    public void Die()
    {
        gameObject.SetActive(false);
    }

    public bool IsDead()
    {
        return !gameObject.activeSelf;
    }

    public void PlayBeatAnimation()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Attack") ||
            animator.GetCurrentAnimatorStateInfo(0).IsName("Die"))
        {
            return;
        }
        animator.Play("Beat");
    }

    private void GradualRotation(float deltaTime)
    {
        Vector3 direction = (lookAtTarget - transform.position).normalized;
        float angleRemaining = Vector3.SignedAngle(transform.forward, direction, transform.up);
        float sign = Mathf.Sign(angleRemaining);
        float angleToRotate = Mathf.Min(rotationSpeed * deltaTime, sign * angleRemaining);
        transform.Rotate(transform.up, sign * angleToRotate);
    }

    private bool Move()
    {
        if (wayPoints.Count == 0)
        {
            return true;
        }
        Vector3 destination = wayPoints[0];
        Vector3 leveled_destination = destination;
        leveled_destination.y = transform.position.y;
        Vector3 targetDirection = (destination - p_transform.position).normalized;
        Quaternion temp = transform.rotation;
        p_transform.LookAt(leveled_destination);
        transform.rotation = temp;
        lookAtTarget = leveled_destination;

        float sqrDiff = Vector3.SqrMagnitude(p_transform.position - destination);
        if (sqrDiff < eps)
        {
            wayPoints.RemoveAt(0);
            return Move();
        }
        p_transform.position += targetDirection * Mathf.Min(moveSpeed, Mathf.Sqrt(sqrDiff));
        return false;
    }

    private void StartAttack()
    {
        if (!Move())
        {
            return;
        }
        timeLeftToAttack -= Time.deltaTime;
        if (timeLeftToAttack > 0)
        {
            return;
        }
        timeLeftToAttack = attackCooldown;
        animator.Play("Attack");
    }
}
