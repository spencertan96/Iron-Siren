﻿using System.Collections.Generic;
using UnityEngine;

class RangeEnemy1 : MonoBehaviour, IEnemy
{
    public Player target;
    public SpikeProjectile projectile;
    //public Sprite sprite;

    private const float rotationSpeed = 100.0f;
    private const float moveSpeed = 0.05f;
    private const float eps = 0.1f;
    private const float initialHealth = 100.0f;

    private Transform p_transform
    {
        get
        {
            return transform.parent;
        }
    }

    private Animator animator;
    private Vector3 projectileLocalPosition;
    private Vector3 lookAtTarget;
    private bool isDying = false;
    private bool isAttacking = false;
    private List<Vector3> wayPoints = new List<Vector3>();
    private Vector3[] patrolPoints;
    private int currentPatrol = 0;
    private float health = initialHealth;


    private void Awake()
    {
        projectileLocalPosition = projectile.transform.localPosition;
    }

    void Update()
    {
        if (isDying)
        {
            return;
        }
        StartAttack();
        GradualRotation(Time.deltaTime);
    }

    public float GetHealth()
    {
        return health;
    }

    public void RestoreHealth()
    {
        wayPoints.Clear();
        health = initialHealth;
        isDying = false;

        gameObject.SetActive(true);
        gameObject.layer = Environment.CollisionLayer;
        animator = gameObject.GetComponentInChildren<Animator>();
        animator.applyRootMotion = false;
        //Apply inputs to animator
        animator.Play("Run");

        lookAtTarget = transform.position + transform.forward;
    }

    public void AddWaypoint(Vector3 point)
    {
        wayPoints.Add(point);
    }

    public void AddPatrolPoints(Vector3[] points)
    {
        patrolPoints = points;
    }

    public void ApplyDamage(float damage)
    {
        if (isDying)
        {
            return;
        }
        health -= damage;
        if (health <= 0)
        {
            animator.Play("Death");
            isDying = true;
            gameObject.layer = Environment.IgnoreLayer;
        }
    }

    public void Attack()
    {
        projectile.ResetProjectile(5.0f, transform.position + transform.TransformVector(projectileLocalPosition), target.transform.position, transform.up, target);
    }

    public void FinishAttack()
    {
        isAttacking = false;
    }

    public void Die()
    {
        gameObject.SetActive(false);
        FinishAttack();
    }

    public bool IsDead()
    {
        return !gameObject.activeSelf && projectile.IsDead();
    }

    public void PlayBeatAnimation()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Attack") ||
            animator.GetCurrentAnimatorStateInfo(0).IsName("Die"))
        {
            return;
        }
        animator.Play("Beat");
    }

    private void GradualRotation(float deltaTime)
    {
        Vector3 direction = (lookAtTarget - transform.position).normalized;
        float angleRemaining = Vector3.SignedAngle(transform.forward, direction, transform.up);
        float sign = Mathf.Sign(angleRemaining);
        float angleToRotate = Mathf.Min(rotationSpeed * deltaTime, sign * angleRemaining);
        transform.Rotate(transform.up, sign * angleToRotate);
    }

    private bool Move()
    {
        if (wayPoints.Count == 0)
        {
            return true;
        }
        Vector3 destination = wayPoints[0];
        Vector3 leveled_destination = destination;
        leveled_destination.y = transform.position.y;
        Vector3 targetDirection = (destination - p_transform.position).normalized;
        Quaternion temp = transform.rotation;
        p_transform.LookAt(leveled_destination);
        transform.rotation = temp;
        lookAtTarget = leveled_destination;

        float sqrDiff = Vector3.SqrMagnitude(p_transform.position - destination);
        if (sqrDiff < eps)
        {
            wayPoints.RemoveAt(0);
            return Move();
        }
        p_transform.position += targetDirection * Mathf.Min(moveSpeed, Mathf.Sqrt(sqrDiff));
        return false;
    }

    private bool CheckPatrol()
    {
        if (patrolPoints == null || isAttacking)
        {
            return true;
        }
        Vector3 destination = patrolPoints[currentPatrol];
        Vector3 leveled_destination = destination;
        leveled_destination.y = transform.position.y;
        Vector3 targetDirection = (destination - p_transform.position).normalized;
        animator.Play("Run");
        Quaternion temp = transform.rotation;
        p_transform.LookAt(leveled_destination);
        transform.rotation = temp;
        lookAtTarget = leveled_destination;
        float sqrDiff = Vector3.SqrMagnitude(p_transform.position - destination);

        if (sqrDiff < eps)
        {
            currentPatrol += 1;
            currentPatrol %= patrolPoints.Length;
            animator.Play("Idle");
            return true;
        }
        p_transform.position += targetDirection * Mathf.Min(moveSpeed, Mathf.Sqrt(sqrDiff));
        return false;
    }

    private void StartAttack()
    {
        if (!Move() || !CheckPatrol())
        {
            return;
        }
        isAttacking = true;

        Vector3 leveled_destination = target.transform.position;
        leveled_destination.y = transform.position.y;
        Quaternion temp = transform.rotation;
        p_transform.LookAt(leveled_destination);
        transform.rotation = temp;
        lookAtTarget = leveled_destination;

        if (projectile.gameObject.activeSelf)
        {
            return;
        }
        animator.Play("Attack");
    }
}
