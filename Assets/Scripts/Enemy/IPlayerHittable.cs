﻿public interface IPlayerHittable
{
    void RestoreHealth();
    float GetHealth();
    void ApplyDamage(float damage);
    bool IsDead();
}
