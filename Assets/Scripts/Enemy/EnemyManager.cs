﻿using System.Collections.Generic;
using UnityEngine;

class EnemyManager : IEnemyManager
{
    private const int numMelee = 4;
    private const int numRange = 2;

    private ISpawn spawn;
    private Player player;
    private List<GameObject> meleeEnemies = new List<GameObject>();
    private List<GameObject> rangeEnemies = new List<GameObject>();

    public EnemyManager(ISpawn spawn, GameObject meleeEnemyTemplate, GameObject rangeEnemyTemplate, Player player)
    {
        this.spawn = spawn;
        this.player = player;
        for (int i = 0; i < numMelee; i++)
        {
            GameObject newEnemy = GameObject.Instantiate(meleeEnemyTemplate);
            newEnemy.SetActive(true);
            meleeEnemies.Add(newEnemy);
        }
        for (int i = 0; i < numRange; i++)
        {
            GameObject newEnemy = GameObject.Instantiate(rangeEnemyTemplate);
            newEnemy.SetActive(true);
            rangeEnemies.Add(newEnemy);
        }
    }

    public bool AreEnemiesDead()
    {
        for (int i = 0; i < numMelee; i++)
        {
            if (!meleeEnemies[i].GetComponentInChildren<IEnemy>(true).IsDead())
            {
                return false;
            }
        }
        for (int i = 0; i < numRange; i++)
        {
            if (!rangeEnemies[i].GetComponentInChildren<IEnemy>(true).IsDead())
            {
                return false;
            }
        }
        return true;
    }

    public void CreateNewEnemies()
    {
        spawn.StartNextWave();
        spawn.MoveMeleeEnemies(player, meleeEnemies, numMelee);
        spawn.GenerateRandomPointsForMelee(player, meleeEnemies, numMelee);
        spawn.MoveRangeEnemies(player, rangeEnemies, numRange);
        spawn.GenerateRandomPointsForRange(player, rangeEnemies, numRange);
    }

    public void PlayBeatAnimation()
    {
        for (int i = 0; i < numMelee; i++)
        {
            meleeEnemies[i].GetComponentInChildren<IEnemy>()?.PlayBeatAnimation();
        }
        for (int i = 0; i < numRange; i++)
        {
            rangeEnemies[i].GetComponentInChildren<IEnemy>()?.PlayBeatAnimation();
        }
    }
}
