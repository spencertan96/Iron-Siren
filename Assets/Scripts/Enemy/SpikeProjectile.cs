﻿using UnityEngine;

class SpikeProjectile : MonoBehaviour, IProjectile
{
    private const float distanceFromPlayer = 0.75f;
    private const float arcHeight = 12.0f;
    private const float spinSpeed = 200.0f;
    private const float damage = 5.0f;
    private const float initialHealth = 50.0f;

    private float fullTime;
    private float timeLeft;
    private float spinTime;
    private float health = initialHealth;

    private IPlayer player;
    private Vector3 originalPosition;
    private Vector3 midPoint;
    private Vector3 endPoint;

    void Update()
    {
        timeLeft -= Time.deltaTime;
        if (timeLeft < 0)
        {
            player?.ApplyDamage(damage);
            gameObject.SetActive(false);
            gameObject.layer = Environment.IgnoreLayer;
            return;
        }

        //Quadratic spline interpolation
        float alpha = timeLeft / fullTime;
        if (alpha < 0 || alpha >= 1)
        {
            alpha = 1;
        }
        Vector3 interp1 = originalPosition * alpha + midPoint * (1 - alpha);
        Vector3 interp2 = midPoint * alpha + endPoint * (1 - alpha);
        transform.position = interp1 * alpha + interp2 * (1 - alpha);
        transform.forward = (interp2 - interp1).normalized;

        spinTime += Time.deltaTime;
        transform.RotateAround(transform.position, transform.forward, spinSpeed * spinTime);
    }

    public void ResetProjectile(float fullTime, Vector3 originalPosition, Vector3 destination, Vector3 up,
        IPlayer player)
    {
        this.fullTime = fullTime;
        timeLeft = fullTime;
        spinTime = 0;
        transform.position = originalPosition;
        this.originalPosition = originalPosition;
        this.player = player;
        RestoreHealth();

        Vector3 direction = (destination - originalPosition).normalized;
        Vector3 right = Vector3.Cross(direction, up).normalized;
        Vector3 tangent = Vector3.Cross(right, direction);
        endPoint = destination - direction * distanceFromPlayer;
        midPoint = (originalPosition + (destination - originalPosition) / 2) + tangent * arcHeight;

        float maxAngle = player.GetCameraVerticalAngle();
        float maxY = Mathf.Tan(maxAngle) * Vector3.Dot(midPoint - player.GetPosition(), player.GetForwardVector());
        midPoint.y = Mathf.Min(maxY, midPoint.y);
    }

    public float GetHealth()
    {
        return health;
    }

    public void RestoreHealth()
    {
        gameObject.SetActive(true);
        gameObject.layer = Environment.CollisionLayer;
        health = initialHealth;
    }

    public void ApplyDamage(float damage)
    {
        health -= damage;
        if (health <= 0)
        {
            timeLeft = 0;
            gameObject.SetActive(false);
            gameObject.layer = Environment.IgnoreLayer;
        }
    }

    public bool IsDead()
    {
        return !gameObject.activeSelf;
    }
}
