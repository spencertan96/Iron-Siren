﻿using System.Collections.Generic;
using UnityEngine;

public interface ISpawn
{
    void StartNextWave();
    void MoveMeleeEnemies(Player player, List<GameObject> meleeEnemies, int numMelee);
    void GenerateRandomPointsForMelee(Player player, List<GameObject> meleeEnemies, int numMelee);
    void MoveRangeEnemies(Player player, List<GameObject> rangeEnemies, int numRange);
    void GenerateRandomPointsForRange(Player player, List<GameObject> rangeEnemies, int numRange);
}
