﻿using UnityEngine;

public class Rhythm1 : MonoBehaviour, IRhythm
{
    private IEnemyManager enemyManager;
    private AudioSource music;
    private AudioSource beatLoop;
    private AudioSource hardHitSound;
    private AudioSource softHitSound;
    private AudioSource[] beatSounds = new AudioSource[8];

    private const float precision = 0.3f;
    private const float delay = 2.0f;

    private const float loopTime = 16.0f * 60.0f / 131.0f;
    //private const float beatLoopTime = 7.32f;
    private float[] beats = { 0.058f, 0.836f, 1.939f, 2.601f, 3.796f, 4.470f, 5.712f, 6.385f };
    private int nextBeatToReset = 1;
    private int nextBeatToPlay = 1;

    private bool hasFired = false;
    private float timeElapsed = -delay;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("PlayMusic", delay);
        InvokeRepeating("BeatLoop", 0.0f + delay, loopTime);
        //InvokeRepeating("ResetCooldown", (beatTime / 2) + delay, beatTime);
        for (int i = 0; i < beats.Length; i++)
        {
            InvokeRepeating("PlayBeatSound", delay + beats[i], loopTime);
        }
    }

    void FixedUpdate()
    {
        timeElapsed += Time.fixedDeltaTime;
        ResetCooldown();
        PlayBeatAnimation();
    }

    public void SetSounds(AudioSource music, AudioSource beatLoop,
        AudioSource hardHitSound, AudioSource softHitSound, AudioSource beatSound)
    {
        this.music = music;
        this.beatLoop = beatLoop;
        this.hardHitSound = hardHitSound;
        this.softHitSound = softHitSound;

        for (int i = 0; i < beatSounds.Length; i++)
        {
            beatSounds[i] = gameObject.AddComponent<AudioSource>();
            beatSounds[i].playOnAwake = false;
            beatSounds[i].loop = false;
            beatSounds[i].clip = beatSound.clip;
        }
    }

    public void SetEnemyManager(IEnemyManager enemyManager)
    {
        this.enemyManager = enemyManager;
    }

    public bool FireWeapon()
    {
        hasFired = true;
        int loops = (int) (timeElapsed / loopTime);
        float diff = timeElapsed - loops * loopTime;
        if (diff > loopTime - precision) diff -= loopTime;
        bool hasBonus = false;
        foreach (float beatTime in beats)
        {
            float lowerBound = beatTime - precision;
            float upperBound = beatTime + precision;
            hasBonus = hasBonus || (diff > lowerBound && diff < upperBound);
        }
        if (hasBonus)
        {
            hardHitSound.Play();
        }
        else
        {
            softHitSound.Play();
        }
        return hasBonus;
    }

    public bool OnBeat()
    {
        return !hasFired;
    }

    private void PlayMusic()
    {
        music.Play();
    }

    private void BeatLoop()
    {
        beatLoop.Play();
    }

    private void PlayBeatSound()
    {
        beatSounds[nextBeatToPlay].Play();
    }

    private void ResetCooldown()
    {
        int loops = (int)(timeElapsed / loopTime);
        float diff = timeElapsed - loops * loopTime;
        float beatTime;
        float upperBound = 0.0f;
        if (nextBeatToReset == 0)
        {
            beatTime = (beats[beats.Length - 1] + beats[nextBeatToReset] + loopTime) / 2;
        }
        else
        {
            beatTime = (beats[nextBeatToReset - 1] + beats[nextBeatToReset]) / 2;
        }
        if (nextBeatToReset == 1)
        {
            upperBound = (beats[nextBeatToReset] + beats[nextBeatToReset + 1]) / 2;
        }
        if (diff >= beatTime && (nextBeatToReset != 1 || diff < upperBound))
        {
            hasFired = false;
            nextBeatToReset += 1;
            nextBeatToReset %= beats.Length;
        }
    }

    private void PlayBeatAnimation()
    {
        int loops = (int)(timeElapsed / loopTime);
        float diff = timeElapsed - loops * loopTime;
        float beatTime = beats[nextBeatToPlay] - precision;
        if (beatTime < 0) beatTime += loopTime;
        if (diff >= beatTime)
        {
            enemyManager.PlayBeatAnimation();
            nextBeatToPlay += 1;
            nextBeatToPlay %= beats.Length;
        }
    }
}
