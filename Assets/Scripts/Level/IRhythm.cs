﻿using UnityEngine;

public interface IRhythm
{
    void SetSounds(AudioSource music, AudioSource beat, AudioSource hardHitSound,
        AudioSource softHitSound, AudioSource beatSound);
    void SetEnemyManager(IEnemyManager enemyManager);
    bool OnBeat();
    bool FireWeapon();
}
