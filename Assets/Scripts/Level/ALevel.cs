﻿using UnityEngine;

public abstract class ALevel: MonoBehaviour
{
    public Environment environment;
    public AudioSource music;
    public AudioSource beatLoop;
    public AudioSource hardHitSound;
    public AudioSource softHitSound;
    public AudioSource beatSound;

    protected ISpawn spawn;
    protected IRhythm rhythm;

    public IRhythm GetRhythm()
    {
        return rhythm;
    }

    public ISpawn GetSpawn()
    {
        return spawn;
    }
}
