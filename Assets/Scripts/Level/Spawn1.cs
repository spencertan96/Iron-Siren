﻿using System.Collections.Generic;
using UnityEngine;

class Spawn1: ISpawn
{
    private const float enemyDistance = 50.0f;
    private const float distanceFromPlayer = 2.0f;
    private const float patrolDistance = 10.0f;

    public void StartNextWave()
    {
        ;
    }

    public void MoveMeleeEnemies(Player player, List<GameObject> meleeEnemies, int numMelee)
    {
        Vector3 point = player.transform.position + player.transform.forward * enemyDistance;
        for (int i = 0; i < numMelee; i++)
        {
            meleeEnemies[i].GetComponentInChildren<IEnemy>(true).RestoreHealth();
            Vector2 randomPoint = Random.insideUnitCircle * Random.Range(25.0f, 50.0f);
            meleeEnemies[i].transform.position = point + new Vector3(randomPoint.x, 0, randomPoint.y);
        }
    }

    public void GenerateRandomPointsForMelee(Player player, List<GameObject> meleeEnemies, int numMelee)
    {
        for (int i = 0; i < numMelee; i++)
        {
            IEnemy enemy = meleeEnemies[i].GetComponentInChildren<IEnemy>(true);
            Transform transform = meleeEnemies[i].transform;
            transform.LookAt(player.transform.position);
            transform.Rotate(transform.up, Random.Range(-15.0f, 15.0f));

            Vector3 direction = (player.transform.position - transform.position);
            float distance = direction.magnitude * 0.9f;
            float randomDistance = Random.Range(distance * 0.2f, distance * 0.8f);
            Vector3 point1 = transform.position + transform.forward * randomDistance;
            enemy.AddWaypoint(point1);
            Vector3 direction2 = (point1 - player.transform.position).normalized;
            enemy.AddWaypoint(player.transform.position + direction2 * distanceFromPlayer);
        }
    }

    public void MoveRangeEnemies(Player player, List<GameObject> rangeEnemies, int numRange)
    {
        Vector3 point = player.transform.position + player.transform.forward * enemyDistance;
        for (int i = 0; i < numRange; i++)
        {
            rangeEnemies[i].GetComponentInChildren<IEnemy>(true).RestoreHealth();
            Vector2 randomPoint = Random.insideUnitCircle * Random.Range(25.0f, 50.0f);
            rangeEnemies[i].transform.position = point + new Vector3(randomPoint.x, 0, randomPoint.y);
        }
    }

    public void GenerateRandomPointsForRange(Player player, List<GameObject> rangeEnemies, int numRange)
    {
        for (int i = 0; i < numRange; i++)
        {
            IEnemy enemy = rangeEnemies[i].GetComponentInChildren<IEnemy>(true);
            Transform transform = rangeEnemies[i].transform;
            transform.LookAt(player.transform.position);
            transform.Rotate(transform.up, Random.Range(-15.0f, 15.0f));

            Vector3 direction = (player.transform.position - transform.position);
            float distance = direction.magnitude * 0.9f;
            float randomDistance = Random.Range(distance * 0.2f, distance * 0.8f);
            Vector3 point1 = transform.position + transform.forward * randomDistance;
            enemy.AddWaypoint(point1);

            Vector3 point2 = point1 + player.transform.right * patrolDistance;
            Vector3 point3 = point1 + player.transform.right * -patrolDistance;
            bool isPoint2Closer = Vector3.SqrMagnitude(point2 - player.transform.position) <
                Vector3.SqrMagnitude(point3 - player.transform.position);
            Vector3[] patrolPoints = new Vector3[2];
            patrolPoints[0] = point1;
            patrolPoints[1] = isPoint2Closer ? point2 : point3;
            enemy.AddPatrolPoints(patrolPoints);
        }
    }
}
