﻿using UnityEngine;

class Level1 : ALevel
{
    void Awake()
    {
        spawn = new Spawn1();
        rhythm = gameObject.AddComponent<Rhythm1>();
        rhythm.SetSounds(music, beatLoop, hardHitSound, softHitSound, beatSound);
        rhythm.SetEnemyManager(environment.GetEnemyManager());
    }

    private void Start()
    {
        rhythm.SetEnemyManager(environment.GetEnemyManager());
    }

    void Update()
    {
    }
}
