﻿using UnityEngine;

public interface IEnvironment
{
    IPlayerHittable TestHit(Ray ray, out Vector3 hitPosition);
    void CreateArrow(GameObject projectilePrefab, Transform arrowLocation, float shotPower);
    IEnemyManager GetEnemyManager();
}
