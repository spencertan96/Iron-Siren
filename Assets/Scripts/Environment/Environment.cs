﻿using System.Collections.Generic;
using UnityEngine;

public class Environment : MonoBehaviour, IEnvironment
{
    public Player player;
    public ALevel level;
    public GameObject treeTemplate;
    public GameObject meleeEnemyTemplate;
    public GameObject rangeEnemyTemplate;
    public const int CollisionLayer = 8;
    public const int IgnoreLayer = 9;

    private const int numTrees = 100;
    private const float distance = 1000.0f;
    private IEnemyManager enemyManager;
    private List<GameObject> trees = new List<GameObject>();

    void Awake()
    {
        InitEnvironment();
        enemyManager = new EnemyManager(level.GetSpawn(), meleeEnemyTemplate, rangeEnemyTemplate, player);
    }

    // Update is called once per frame
    void Update()
    {
        if (enemyManager.AreEnemiesDead())
        {
            enemyManager.CreateNewEnemies();
        }
    }

    private void InitEnvironment()
    {
        for (int i = 0; i < numTrees; i++)
        {
            GameObject newTree = Instantiate(treeTemplate);
            trees.Add(newTree);
            newTree.SetActive(true);
            newTree.layer = CollisionLayer;
            newTree.transform.position = new Vector3(Random.Range(-150.0f, 150.0f),
                0, Random.Range(-200.0f, 200.0f));
            float randomScale = Random.Range(0.3f, 1.5f);
            newTree.transform.localScale = new Vector3(randomScale, randomScale, randomScale);
            newTree.transform.Rotate(newTree.transform.up, Random.Range(-180.0f, 180.0f));
        }
    }

    public IPlayerHittable TestHit(Ray ray, out Vector3 hitPosition)
    {
        RaycastHit result;
        bool hasHit = Physics.Raycast(ray.origin, ray.direction, out result, distance, 1 << CollisionLayer);
        hitPosition = ray.origin + ray.direction * distance;
        if (!hasHit)
        {
            return null;
        }
        else
        {
            hitPosition = result.point;
            return result.collider.gameObject.GetComponentInChildren<IPlayerHittable>(true);
        }
    }

    public void CreateArrow(GameObject projectilePrefab, Transform arrowLocation, float shotPower)
    {
        GameObject projectile = Instantiate(projectilePrefab);
        projectile.SetActive(true);
        projectile.transform.position = arrowLocation.position;
        projectile.transform.rotation = arrowLocation.rotation;
        projectile.GetComponent<Rigidbody>().AddRelativeForce(arrowLocation.forward * shotPower);
    }

    public IEnemyManager GetEnemyManager()
    {
        return enemyManager;
    }
}
