﻿Shader "Sprites/HealthBarShader"
{
	Properties
	{
	   [PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
		[Header(Life)]_Color("Main Color", Color) = (0.2,1,0.2,1)
		_LowHealthColor("Low Health Color", Color) = (0.2,1,0.2,1)
		_Percent("Percent", Float) = 1

		[Header(Damages)]_DamageColor("Damage color", Color) = (1,1,0,1)
		_DamagePercent("Damage Percent", Float) = 0

		[MaterialToggle] PixelSnap("Pixel snap", Float) = 0


		_ImageSize("Image Size", Vector) = (100, 100, 0, 0)
	}

		SubShader
		{
			Tags
			{
				"Queue" = "Transparent"
				"IgnoreProjector" = "True"
				"RenderType" = "Transparent"
				"PreviewType" = "Plane"
				"CanUseSpriteAtlas" = "True"
			}

			Cull Off
			Lighting Off
			ZWrite Off
			Blend One OneMinusSrcAlpha

			Pass
			{
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma multi_compile _ PIXELSNAP_ON
				#include "UnityCG.cginc"

				struct appdata_t
				{
					float4 vertex   : POSITION;
					float4 color    : COLOR;
					float2 texcoord : TEXCOORD0;
				};

				struct v2f
				{
					float4 vertex   : SV_POSITION;
					fixed4 color : COLOR;
					half2 texcoord  : TEXCOORD0;
				};

				fixed4 _Color;
				fixed4 _LowHealthColor;
				half _Percent;

				fixed4 _DamageColor;
				half _DamagePercent;

				v2f vert(appdata_t IN)
				{
					v2f OUT;
					OUT.vertex = UnityObjectToClipPos(IN.vertex);
					OUT.texcoord = IN.texcoord;
					#ifdef PIXELSNAP_ON
					OUT.vertex = UnityPixelSnap(OUT.vertex);
					#endif

					return OUT;
				}

				sampler2D _MainTex;
				float4 _ImageSize;

				fixed4 frag(v2f IN) : SV_Target
				{
					fixed4 c = tex2D(_MainTex, IN.texcoord);
					
					if (IN.texcoord.y > 0.8)
					{
						c.rgb *= 1.4;
					}
					else if (IN.texcoord.y < 0.2)
					{
						c.rgb *= 0.6;
					}
					else
					{
						c.rgb *= (1 + (IN.texcoord.y - 0.5) / (0.8 - 0.2) * 0.8);
					}

					if (IN.texcoord.x > _Percent + _DamagePercent)
					{
					   c.a = 0;
					}
					else
					{
						if (IN.texcoord.x > _Percent)
						   c *= _DamageColor;
						else
						{
						   c *= (_Color * _Percent + _LowHealthColor * (1 - _Percent));
						}
					}


					c.rgb *= c.a;
					return c;
				}
			ENDCG
			}
		}
}